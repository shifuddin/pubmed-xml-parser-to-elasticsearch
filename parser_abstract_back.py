import os, os.path
import pubmed_parser as pp
from langdetect import detect
import json
import elasticsearch
import logging

def main():
	try:
		list_abstract_xml = pp.list_xml_path('/home/spuser/datastore/abstracts')
		es = elasticsearch.Elasticsearch()
	
		by_ID = 0
		total = 0
		for num in range (0, len(list_abstract_xml)-1):
			pubmed_abstract_dict_list = pp.parse_medline_xml(list_abstract_xml[num])
			total += len(pubmed_abstract_dict_list)
			count = 1
			for pubmed_abstract_dict in pubmed_abstract_dict_list:
				
				PMID = ""
				PMC = ""
				ID = ""
				ABSTRACT = ""
				TITLE = ""
				AUTHORS = []
				PUBDATE = ""
				JOURNAL = ""
				try:
					PMID = pubmed_abstract_dict['pmid']
					PMC = pubmed_abstract_dict['pmc']
					TITLE = pubmed_abstract_dict['title']
					ABSTRACT = pubmed_abstract_dict['abstract']
					AUTHORS = pubmed_abstract_dict['authors']
	
					PUBDATE = pubmed_abstract_dict['pubdate']
					JOURNAL = pubmed_abstract_dict['journal']
					print "Here"
					if len(PMID) == 0:
						raise Exception("PMID")
					elif len(PMC) == 0:
						raise Exception ("PMC")
					elif len(PMID) == 0 and len(PMC) == 0:
						raise Exception ("BOTH")
					by_ID += 1
					ID = PMID +"_" +PMC[PMC.rfind('C')+1:]
					try:
						res = es.get(index="pubmed", doc_type = "fulltext", id =ID )
						#print res
					except Exception as e:
						try:
							print e	
						except ValueError as e:
							print "Here " +str(e)
						
				except Exception as e:
					if "PMC" in str(e):
						res = es.search(index="pubmed", body={"query": {"match": {'pmid':PMID}}})
						hits = res['hits']
						if hits['total'] == 0:
							print "Not found"
				if count == 50:
					break
				count += 1					
			break
		print " Total " + str (total) + " Found " + str(by_ID)
	except OSError as e:
		print str(e)
		

if __name__ == "__main__":
    main()
