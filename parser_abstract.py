'''
__author__ = "Shifuddin"
__copyright__ = "Copyright 2017, DDM-Engineers Team DDM LAB"
__filename__ = "parser_fulltext.py"
__date__ = "7/18/2017"
__purpose__ = "Parse abstract xml files. Add to elasticsearch if not already existed"
'''
from  __future__ import division
import os, os.path
import pubmed_parser as pp
from langdetect import detect
import json
import elasticsearch
import sys
reload(sys)
sys.setdefaultencoding("utf8")


def main():
	""" Main entry point of the parser
	"""
	try:
		# load list of xmls in the path
		list_abstract_xml = pp.list_xml_path('/home/spuser/datastore/abstracts')

		# create elasticsearch object
		es = elasticsearch.Elasticsearch([{"host":'10.155.208.78', 'port': 9200}])
		
		es_search_index = 'pubmed_ready'
		es_search_doc = 'citations'
		# some counters to tract the progress of parsing
		entries_per_xml = 0
		already_existed = 0
		already_existed_pmid = 0
		already_existed_pmc = 0
		inserted_as_abstract = 0
		updated_pubmed_fulltext = 0

		# some global variables which are set to empty during upload
		PROTS = []
		LOCS = []
		PROTLOCS = []

		
		# open file to write the progremm of parsing
		f = open("pubmed_abstract.txt","a")

		print "Total abstract xml "+ str(len(list_abstract_xml))
		
		# take argument from which document parsing should start		
		try:
			starting_xml = int (sys.argv[1])
			end_xml = int (sys.argv[2])
		except Exception as e:
			print "Give two integer argument"
			return
	
		
		for num in range (starting_xml, end_xml):
			
			# parse using medline xml method
			pubmed_abstract_dict_list = pp.parse_medline_xml(list_abstract_xml[num])
			entries_per_xml = len(pubmed_abstract_dict_list)
			print "	" + list_abstract_xml[num]   + " has "  + str(entries_per_xml) + " entries"
			count = 1
			
			# calculate percen of completed task to show
			percent = (num/len(list_abstract_xml)) * 100
			
			# each xml file contains 30000 of docs, so we need to iterate over
			for pubmed_abstract_dict in pubmed_abstract_dict_list:
				
				PMID = ""
				PMC = ""
				ID = ""
				ABSTRACT = ""
				TITLE = ""
				AUTHORS = []
				PUBDATE = ""
				JOURNAL = ""
				parsing_error = False
				add_info = ""
				LANGUAGE = ""
				REST = ""
				DOI = ""
				try:
					# Parsing the fields
					PMID = pubmed_abstract_dict['pmid']
					PMC = pubmed_abstract_dict['pmc']
					TITLE = pubmed_abstract_dict['title']
					ABSTRACT = pubmed_abstract_dict['abstract']
					AUTHORS = pubmed_abstract_dict['author']
	
					PUBDATE = pubmed_abstract_dict['pubdate']
					JOURNAL = pubmed_abstract_dict['journal']
					
					# parsing language 
					try:
						LANGUAGE = detect(TITLE)
					except Exception as e:
						try:
							LANGUAGE = detect(ABSTRACT)
						except Exception as e:
							LANGUAGE = "unknown"
			
					print "		Processing " + str(count) + " of " + str(entries_per_xml) + " ( completed total: " + str(percent)  + "% )"
					# if no pmid then raise exception as PMID
					if len(PMID) == 0:
						raise Exception("PMID")
					# if no pmc then raise exception as PMC
					elif len(PMC) == 0:
						raise Exception ("PMC")
					# if no pmc and pmid then as BOTH
					elif len(PMID) == 0 and len(PMC) == 0:
						raise Exception ("BOTH")
					
					# merge both to get ID
					ID = PMID +"_" +PMC[PMC.rfind('C')+1:]
					
					try:
						# get document from ID
						res = es.get(index=es_search_index, doc_type = es_search_doc, id =ID )
						# if above line does not raise exception we have the document
						print "		Alredy existed as ID: " + ID
						already_existed += 1
						data = {}
						updated = 0
						
						# we try to find out if anything missing in the already existing document
						# if missing we try to update that from data from abstract document 
						# this could be a redundant check but useful when we have something missing
						# in the central document and have in the abstract

						if len(res['_source']['abstract']) == 0:
							data={'pmid': res['_source']['pmid'], 'pmc':res['_source']['pmc'], 'title':res['_source']['title'], 'abstract': ABSTRACT, 'author':res['_source']['author'], 'journal': res['_source']['journal'], 'doi': res['_source']['doi'], 'parsing_error':res['_source']['parsing_error'], 'published': res['_source']['published'], 'language': res['_source']['language'], 'rest': res['_source']['rest'], 'additional_info': res['_source']['additional_info'], "prots": PROTS, "locs": LOCS, "protlocs": PROTLOCS}
							updated = 1
						if len(res['_source']['title']) == 0:
							data={'pmid': res['_source']['pmid'], 'pmc':res['_source']['pmc'], 'title':TITLE, 'abstract': res['_source']['abstract'], 'author':res['_source']['author'], 'journal': res['_source']['journal'], 'doi': res['_source']['doi'], 'parsing_error':res['_source']['parsing_error'], 'published': res['_source']['published'], 'language': res['_source']['language'], 'rest': res['_source']['rest'], 'additional_info': res['_source']['additional_info'], "prots": PROTS, "locs": LOCS, "protlocs": PROTLOCS}
							updated = 1
						if len(res['_source']['abstract']) == 0 and len(res['_source']['title']) == 0:
							data={'pmid': res['_source']['pmid'], 'pmc':res['_source']['pmc'], 'title':TITLE, 'abstract': ABSTRACT, 'author':res['_source']['author'], 'journal': res['_source']['journal'], 'doi': res['_source']['doi'], 'parsing_error':res['_source']['parsing_error'], 'published': res['_source']['published'], 'language': res['_source']['language'], 'rest': res['_source']['rest'], 'additional_info': res['_source']['additional_info'], "prots": PROTS, "locs": LOCS, "protlocs": PROTLOCS}
							updated = 1
						if updated == 1:
							json_entry = json.dumps(data)
							es.index(index = es_search_index, doc_type=es_search_doc, id=ID, body=json_entry)
							updated_pubmed_fulltext += 1
					
					# this exception raised when we have PMID and PMC both but no such document as 
					# ID = PMID+ "_" + PMC		
 					except Exception as e:
						# if we have no abstract or title then parsing_error will be sat true
						try:
							if len(ABSTRACT) == 0:
								parsing_error = True
								add_info = "No abstract"
							if len(TITLE) == 0:
								parsing_error = True
								add_info = "No title" 
							# create doc
							data = {"pmid": PMID, "pmc": PMC, "title": TITLE, "abstract": ABSTRACT, "author": str(AUTHORS), "journal":JOURNAL, "doi":DOI,  "parsing_error":parsing_error, "published": PUBDATE, "language": LANGUAGE, "rest":REST, "additional_info":add_info, "prots": PROTS, "locs": LOCS, "protlocs": PROTLOCS}
							json_entry = json.dumps(data)
							
							# upload to elasticsearch
							res = es.index(index= es_search_index, doc_type=es_search_doc, id=ID, body=json_entry)	
							print "		Added (Create: " + str(res['created'])+ " ) by ID(PMID_PMC) as " + ID
							inserted_as_abstract += 1
						except Exception as e:
							print str(e)
							
						
				except Exception as e:
					if "PMC" in str(e):
						# if there is no PMC we seach with pmid
						res = es.search(index=es_search_index, body={"query": {"match": {'pmid':PMID}}})
						hits = res['hits']
						
						# if no document add as new
						if hits['total'] == 0:
							if len(ABSTRACT) == 0:
                                                                parsing_error = True
                                                                add_info = "No abstract"
                                                        if len(TITLE) == 0:
                                                                parsing_error = True
                                                                add_info = "No title"
							ID = PMID + "_" + PMC
			
                                                       	data = {"pmid": PMID, "pmc": PMC, "title": TITLE, "abstract": ABSTRACT, "author": str(AUTHORS), "journal":JOURNAL, "doi":DOI,  "parsing_error":parsing_error, "published": PUBDATE, "language": LANGUAGE, "rest":REST, "additional_info":add_info, "prots": PROTS, "locs": LOCS, "protlocs": PROTLOCS}

                                                        json_entry = json.dumps(data)
                                                        res = es.index(index=es_search_index, doc_type=es_search_doc, id=ID, body=json_entry)
							print "		Added (Created: " + str(res['created'])+ " ) by ID(PMC missing) as " + PMID
							inserted_as_abstract += 1
						else:
							# just show this is alread added
							if len(ABSTRACT) == 0:
                                                                parsing_error = True
                                                                add_info = "No abstract"
                                                        if len(TITLE) == 0:
                                                                parsing_error = True
                                                                add_info = "No title"

							ID = PMID + "_" + PMC

                                                        data = {"pmid": PMID, "pmc": PMC, "title": TITLE, "abstract": ABSTRACT, "author": str(AUTHORS), "journal":JOURNAL, "doi":DOI,  "parsing_error":parsing_error, "published": PUBDATE, "language": LANGUAGE, "rest":REST, "additional_info":add_info, "prots": PROTS, "locs": LOCS, "protlocs": PROTLOCS}

                                                        json_entry = json.dumps(data)
                                                        res = es.index(index=es_search_index, doc_type=es_search_doc, id=ID, body=json_entry)
                                                        print "         Added (Created: " + str(res['created'])+ " ) by ID(PMC missing) as " + PMID

							already_existed_pmid += 1
							print "		Already existed  as PMID: " + PMID
					# if no PMID search with PMC 
					elif "PMID" in str(e):
                                                res = es.search(index=es_search_index, body={"query": {"match": {'pmc':PMC}}})
                                                hits = res['hits']
						# if no document add as new
                                                if hits['total'] == 0:
                                                        if len(ABSTRACT) == 0:
                                                                parsing_error = True
                                                                add_info = "No abstract"
                                                        if len(TITLE) == 0:
                                                                parsing_error = True
                                                                add_info = "No title"
                                                        ID = PMID + "_" + PMC

                                                        data = {"pmid": PMID, "pmc": PMC, "title": TITLE, "abstract": ABSTRACT, "author": str(AUTHORS), "journal":JOURNAL, "doi":DOI,  "parsing_error":parsing_error, "published": PUBDATE, "language": LANGUAGE, "rest":REST, "additional_info":add_info, "prots": PROTS, "locs": LOCS, "protlocs": PROTLOCS}

                                                        json_entry = json.dumps(data)
                                                        res = es.index(index=es_search_index, doc_type= es_search_doc, id=ID, body=json_entry)
                                                        print "         Added (Created: " + str(res['created'])+ " ) by ID(PMID missing) as " + PMC
                                                        inserted_as_abstract += 1
                                                else:
							# if alread just show it's added
							if len(ABSTRACT) == 0:
                                                                parsing_error = True
                                                                add_info = "No abstract"
                                                        if len(TITLE) == 0:
                                                                parsing_error = True
                                                                add_info = "No title"
							
							ID = PMID + "_" + PMC

                                                        data = {"pmid": PMID, "pmc": PMC, "title": TITLE, "abstract": ABSTRACT, "author": str(AUTHORS), "journal":JOURNAL, "doi":DOI,  "parsing_error":parsing_error, "published": PUBDATE, "language": LANGUAGE, "rest":REST, "additional_info":add_info, "prots": PROTS, "locs": LOCS, "protlocs": PROTLOCS}

                                                        json_entry = json.dumps(data)
                                                        res = es.index(index=es_search_index, doc_type=es_search_doc, id=ID, body=json_entry)
                                                        print "         Added (Created: " + str(res['created'])+ " ) by ID(PMID missing) as " + PMC

                                                        already_existed_pmc += 1
                                                        print "         Already existed  as PMC: " + PMC
					else:
						# if we have no PMID and PMC just show the error
						print str(e)
				
				count += 1				
			f.write("File Name: " + str(list_abstract_xml[num]) + " Number: "+ str(num)+" \n")
		# write some stats to understand the progress of parsing
		f.write(".......................................................\n")
		f.write("Matched with existing pubmed central " + str(already_existed)+"\n")
		f.write("Updated pubmed fulltext " + str(updated_pubmed_fulltext)+"\n")
		f.write("Matched with existing pubmed abstract as PMID" + str(already_existed_pmid) + "\n")
		f.write("Matched with existing pubmed abstract as PMC" + str(already_existed_pmc)+ "\n")
		f.write ("Inserted into abstract " + str(inserted_into_abstract) + "\n")
		f.close()
	except OSError as e:
		print str(e)
		

if __name__ == "__main__":
    main()
