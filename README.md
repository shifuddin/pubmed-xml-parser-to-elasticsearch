
## Pubmed parser to elasticsearch (Motivation)

I have created this parser for one of my education projects. Where I used existing pubmed parser (https://github.com/titipata/pubmed_parser) and customized it according to 
our need. My intention was to parse pubmed abstracts, full text and add to elasticsearch one by one. 

## Output format
From a product manager perspective, the task can be summarized as:


### Input 
PubMed abstracts and PMC (PubMed Central) full texts
### Output 
Elasticsearch index with parsed pubmed abstract and pubmed fulltext.


#### Requirement 
The only requirement is that Elasticsearch index, thus schema, should support the possibly future search engine. For this, we need a document schema that at least (you are free to provide your own additions/suggestions), supports this info:



##### pmid 
(PubMed ID of the article, if any)

##### pmc 
(PubMed Central ID of the article, if any)

##### id 
(unique id of the document. Note that either pmid or pmc or both can exist for a document. Few PMC articles will have no corresponding PubMed identifier, but they do exist, which complicates the decision for an unique identifier. Then, a possible dummy generation of an unique id could be concatenating the other ids, e.g. id = pmid + "_" + pmc. For a document, a non-existing pmid or pmc could be represented as the empty string.

##### title 
(document's title. The assumption is that every document has a title)

##### abstract 
(document's abstract. Note that some abstracts allow have a hierarchy of subsections; example). You can concatenate all the various text parts into a single string by simple space, newline, or double new line, as you see fit. -- Also note that, the fewer but existing, some documents will have no abstract at all).

##### rest 
(document's full text, if any. Again, you will have to concatenate the text parts of the entire document as you see fit. Very often, this kind of "rest" field is rather a gather-all-in field for all the document's text, thus also including title and abstract. Probably, you want this too, but can decide otherwise. This is of relevance for how to later store the string offsets of the text-mined protein locations; see below).

##### language 
(document's original language. Although often English, this may not be the case. You should make sure that you always use the same standard language identifiers, e.g. for English maybe en, but really always "en", and not "English", or "english", or "eng", or "en/US", etc.

##### published 
(document's publication date. The year must or should always exist. If you also have the information of month and/or day, you must included it too).

##### authors 
(list of author names. This is a multivalue field)

##### journal 
(name of the journal where the paper was published in, e.g. "Bioinformatics")

##### doi 
(document's doi, if any. Juanmi's assumption is that some papers, specially older ones, will have no doi. Most recent publications should have one)

##### parsing_error 
(Boolean. If there was a parsing error in the pubmed abstract / full text or not. If you have parsing error in full text, try to get the title&abstract and the pmc)

## Usages

#### Install elasticsearch

pip install elasticsearch

#### Install other dependencies

pip install -r requirements.txt

#### Install packages

python setup.py install

#### Run parser

##### Abstract

python parser_abstract.py starting_point ending_point

python parser_fulltext.py starting_point ending_point




