import os, os.path
import elasticsearch
def SubDirPath (d):
        return filter(os.path.isdir, [os.path.join(d,f) for f in os.listdir(d)])
def main():
	es = elasticsearch.Elasticsearch()
	res = es.search(index="pbmed",doc_type ="art2", body={"query": {"match_all": {}}, "fields": []})
	print res
	id_list = res['hits']['hits']
	for ID in id_list:
		print ID['_id']
	
if __name__ == "__main__":
    main()

