from __future__ import division
import os, os.path
import pubmed_parser as pp
from langdetect import detect
import json
import elasticsearch
import logging
import sys
def SubDirPath (d):
	return filter(os.path.isdir, [os.path.join(d,f) for f in os.listdir(d)])
def setUplog():
	logging.basicConfig(filename='pubmed_central.log',level=logging.WARNING)
def main():
	
	try:
		setUplog()
	except Exception as e:
		raise IOError("Can not set up log")
	try:	
		central_type_iterator = 1
                start_index = 0
                try:
                        central_type_iterator = int(sys.argv[1])
                        start_index = int (sys.argv[1])-1
                except Exception:
                        print "Give int arg"
                        return

		list_central_types = SubDirPath('/home/spuser/datastore/fulltext')
		es = elasticsearch.Elasticsearch()
		total_type = str(len(list_central_types))
		
		print "Total Types: " + total_type
		
		for num in range(start_index, len(list_central_types)-1):
			print "	Processing "+ list_central_types[num][list_central_types[num].rfind('/')+1:] + " "+ str(central_type_iterator) + " of " + total_type
			list_xml = pp.list_xml_path(list_central_types[num])
			print "	Total XML: " + str(len(list_xml ))
			xml_iterator = 1
			for xml in list_xml:
				pubmed_central = pp.parse_pubmed_xml(xml)
				percent_complete = (central_type_iterator/7441)*100
				print "		Processing " + str(xml_iterator)+ " of " + str(len(list_xml))
				
				parsing_error = False
				add_info = []
				PMID = ""
				PMC = ""
				ID = ""
				TITLE = ""
				ABSTRACT = ""
				AUTHOR_LIST = []
				LANGUAGE = ""
				JOURNAL = ""
				DOI = ""
				PUBLISHED = ""
				REST = ""
				try:
					PMID = pubmed_central['pmid']
				except Exception as e:
					add_info.append(str(e))
				try:
					PMC = pubmed_central['pmc']
				except Exception as e:
                                        add_info.append(str(e))
				
				ID = PMID+"_"+PMC
                      
				if len(ID) == 0:
                                	raise ValueError('PMID & PMC not available')        

				try:
					TITLE = pubmed_central['full_title'] 
				except Exception as e:
                                        add_info.append(str(e))
				
				try:
                                        ABSTRACT = pubmed_central['abstract']
                                except Exception as e:
                                        parsing_error = True
                             		add_info.append(str(e))

				try:
                                        LANGUAGE =  detect(TITLE)
                                except Exception as e:
                                        try:
                                                LANGUAGE = detect(ABSTRACT)
                                        except Exception as e:
                                                LANGUAGE = "unknown"
	                                        add_info.append(str(e))
				try:
					AUTHOR_LIST = pubmed_central['author_list']
				except Exception as e:
					add_info.append(str(e))

				try:
					JOURNAL = pubmed_central['journal']
				except Exception as e:
					add_info.append(str(e))
				try:
                                        DOI = pubmed_central['doi']
                                except Exception as e:
                                        add_info.append(str(e))
				
				try:
                                        PUBLISHED = pubmed_central['publication_year']
                                except Exception as e:
                                        add_info.append(str(e))

				REST = TITLE + ABSTRACT
			
					

				# START PARSING PARAGRAPH BY SETTING ALL PARAGRAPH FLAG TRUE		
				pubmed_central_paragraph_list = pp.parse_pubmed_paragraph(xml, all_paragraph=True)
			
				text = ""
				
				text_not_found = 0
				for paragraph in pubmed_central_paragraph_list:
					try:
						text  = text + paragraph['text']
					except Exception as e:
						text_not_found += 1
				
				if text_not_found == len(pubmed_central_paragraph_list):
					parsing_error = True
					add_info.append("Text not found")


				REST = REST + text
				

				data = {"pmid": PMID, "pmc": PMC, "title": TITLE, "abstract": ABSTRACT, "author": str(AUTHOR_LIST), "journarl":JOURNAL, "doi": DOI, "parsing_error":parsing_error, "published": PUBLISHED, "language": LANGUAGE, "rest": REST,"additional_info":add_info}
				
				json_entry = json.dumps(data)			
				es = elasticsearch.Elasticsearch()
				
				try:
					res = es.index(index='pubmed', doc_type='fulltext', id= ID, body= json_entry)		
					print "		Added (Created: "+str(res['created'])+") "+ xml[xml.rfind('/')+1:] + " as " + ID + " (Completed: " + str(percent_complete)  + " %)"  
				except Exception as e:
					print str(e)
				#print es.get(index='pubmed', doc_type= 'fulltext', id=ID)
				
				except ValueError as e:
					logging.warning(str(e))
				xml_iterator += 1
				
			central_type_iterator += 1

	except OSError as e:
                print  str(e)
	except IOError as e:
		print str(e)

if __name__ == "__main__":
    main()
