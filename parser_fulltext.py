'''
__author__ = "Shifuddin"
__copyright__ = "Copyright 2017, DDM-Engineers Team DDM LAB"
__filename__ = "parser_fulltext.py"
__date__ = "7/18/2017"
__purpose__ = "parse xml from pubmed central and add as fulltext document in the elasticsearch server"
'''
from __future__ import division
import os, os.path
import pubmed_parser as pp
from langdetect import detect
import json
import elasticsearch
import logging
import sys

#elasticsearch object
es = elasticsearch.Elasticsearch([{'host': '10.155.208.78', 'port': 9200}])

# some global variables which are set to empty during upload
PROTS = []
LOCS = []
PROTLOCS = []


def SubDirPath (d):
	""" Returns all the subdirectories from a particular directory
	    Central data are divided into several subdirectories
	"""
	return filter(os.path.isdir, [os.path.join(d,f) for f in os.listdir(d)])
def setUplog():
	""" Setup the looging option to keep tract of any waring from elasticsearch
	"""
	logging.basicConfig(filename='pubmed_central.log',level=logging.WARNING)

def createAndPostDocument(xml):
	""" Create elasticsearch document from xml input
	    Parse the document using parse_pubmed_xml method
	    Parse the document using parse_pubmed_paragraph for fulltext
	    Iterate over each section in the fulltext to get all text
	    Create json object and add to elastic search
	"""
	# parsing using first method
	pubmed_central = pp.parse_pubmed_xml(xml)
	
	# fields of json doc				
	parsing_error = False
	add_info = []
	PMID = ""
	PMC = ""
	ID = ""
	TITLE = ""
	ABSTRACT = ""
	AUTHOR_LIST = []
	LANGUAGE = ""
	JOURNAL = ""
	DOI = ""
	PUBLISHED = ""
	REST = ""	
	
	# Parsing inside try block
	# So that we can catch the exception and additional info where we failed	
	try:
		PMID = pubmed_central['pmid']
	except Exception as e:
		add_info.append(str(e))
	
	try:
		PMC = pubmed_central['pmc']
	except Exception as e:
        	add_info.append(str(e))
				
	ID = PMID+"_"+PMC
        
	if len(ID) == 1:
		logging.warning("No PMID & PMC")	
        	return None       

	try:
		TITLE = pubmed_central['full_title'] 
	except Exception as e:
        	add_info.append(str(e))
				
	# if we face problem in parsing abstract we set parsing error as True
	try:
        	ABSTRACT = pubmed_central['abstract']
        except Exception as e:
        	parsing_error = True
                add_info.append(str(e))

	# Language is detected by landetect.
	# First tried with title if fails to detectfrom abstract
	# If both fails set as unknown
	try:
        	LANGUAGE =  detect(TITLE)
        except Exception as e:
		try:
       			LANGUAGE = detect(ABSTRACT)
		except Exception as e:
        		LANGUAGE = "unknown"
	        	add_info.append(str(e))
	try:
		AUTHOR_LIST = pubmed_central['author_list']
	except Exception as e:
		add_info.append(str(e))

	try:
		JOURNAL = pubmed_central['journal']
	except Exception as e:
		add_info.append(str(e))
	try:
        	DOI = pubmed_central['doi']
        except Exception as e:
        	add_info.append(str(e))
				
	try:
        	PUBLISHED = pubmed_central['publication_year']
        except Exception as e:
        	add_info.append(str(e))

	# here rest is title plus abstract
	REST = TITLE + ABSTRACT
			
					

	# START PARSING PARAGRAPH BY SETTING ALL PARAGRAPH FLAG TRUE		
	pubmed_central_paragraph_list = pp.parse_pubmed_paragraph(xml, all_paragraph=True)
			
	text = ""
				
	text_not_found = 0
	
	for paragraph in pubmed_central_paragraph_list:
		try:
			text  = text + paragraph['text']
		except Exception as e:
			text_not_found += 1
	# if we have text not found exception as the number of section in the document then
	# We can tell we have problem in parsing the fulltext			
	if text_not_found == len(pubmed_central_paragraph_list):
		parsing_error = True
		add_info.append("Text not found")


	# here rest is rest plus fulltext
	REST = REST + text
				
	# creating the document to add
	data = {"pmid": PMID, "pmc": PMC, "title": TITLE, "abstract": ABSTRACT, "author": AUTHOR_LIST, "journal":JOURNAL, "doi": DOI, "parsing_error":parsing_error, "published": PUBLISHED, "language": LANGUAGE, "rest": REST,"additional_info":add_info, "prots": PROTS, "locs": LOCS, "protlocs": PROTLOCS}
	
	# dumps as json object
	json_entry = json.dumps(data)
	
	# upload to elasticsearch	
	try:
        	res = es.index(index='pubmed_ready', doc_type='citations', id= ID, body= json_entry)
                print "         Added (Created: "+str(res['created'])+") "+ xml[xml.rfind('/')+1:] + " as " + ID
	except Exception as e:
		# if exception print and log the cause
		print str(e)
             	logging.warning(str(e))
											
def main():
	
	""" Main entry of parser
	"""	
	try:
		setUplog()
	except Exception as e:
		raise IOError("Can not set up log")
	
	# There are some 7411 xmls 
	# We take from which document we are going to start parsing as command line argument
	try:	
                start_index = 0
                try:
                        start_index = int (sys.argv[1])-1
			end_index = int (sys.argv[2])
                except Exception:
                        print "Give int arg"
                        return

		# here is fulltext directory
		list_central_types = SubDirPath('/home/spuser/datastore/fulltext')
	
		
		total_type = end_index-start_index
		
		# print how many subdirectories we have
		print "Total Types: " + str(total_type)
		
		# iterate over each subdirectory, fetch the xmls, parse and add to elastic search
		for num in range(start_index, end_index):
			print "	Processing "+ list_central_types[num][list_central_types[num].rfind('/')+1:] + " "+ str(num+1) + " of " + str(total_type)
			list_xml = pp.list_xml_path(list_central_types[num])
			print "	Total XML: " + str(len(list_xml ))
			xml_iterator = 1
			percent_complete = ((num+1)/total_type)*100
			for xml in list_xml:
				
				print "		Processing " + str(xml_iterator)+ " of " + str(len(list_xml)) + " (Completed: "+ str(percent_complete) + "% - Current Directory- "+ str((num+1))+" )"
				#print createDocument(xml)
				createAndPostDocument(xml)
				xml_iterator += 1	
		
	except OSError as e:
                print  str(e)
	except IOError as e:
		print str(e)

if __name__ == "__main__":
    main()
