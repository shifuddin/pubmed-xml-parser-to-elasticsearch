#!/usr/bin/env python
import elasticsearch
import elasticsearch.helpers
elasticSource = elasticsearch.Elasticsearch([{"host": "localhost", "port": 9200}])
elasticDestination = elasticsearch.Elasticsearch([{"host": "localhost", "port": 9200}])
# Setup source and destinations connection to Elasticsearch. Could have been different clusters
# Delete index so we know it doesn't exist.
# elasticDestination.indices.delete(index="index_destination", ignore=[400, 404])
# Create index with nothing in it.
elasticDestination.indices.create(index="pbmed_new", ignore=[400, 404])
elasticsearch.helpers.reindex(client=elasticSource, source_index="pbmed", target_index="pbmed_new", target_client=elasticDestination)

